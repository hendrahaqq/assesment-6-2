package main

import (
	"assesment-6-2/app/config"
	"assesment-6-2/app/controller"
	"assesment-6-2/app/models"

	"github.com/gin-gonic/gin"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	// koneksi
	db := config.Connect()
	strDB := controller.StrDB{DB: db}
	// migration
	models.Migrations(db)

	router := gin.Default()
	v1 := router.Group("/api/v1")
	{
		// office
		v1.POST("/office", strDB.CreateOffice)
		v1.DELETE("/office/:id", strDB.DeleteOffice)
		v1.PUT("/office/:id", strDB.UpdateOffice)
		v1.GET("/office-show/:current/:perpage", strDB.GetOffice)
		v1.GET("/office/:id/show", strDB.GetOneOffice)
		v1.GET("/office-search", strDB.GetSearchOffice)
		// assesment-6
		v1.GET("/office-users/:id", strDB.GetOfficeUsers) //get all user in an office

		// user
		v1.POST("/user", strDB.CreateUser)
		v1.DELETE("/user/:id", strDB.DeleteUser)
		v1.PUT("/user/:id", controller.Auth, strDB.UpdateUser)
		v1.GET("/user/", strDB.GetUser)
		v1.GET("/user/:id/show", strDB.GetOneUser)
		v1.GET("/user-search", strDB.GetSearchUser)
		v1.POST("/login", strDB.LoginUser)
		//assesment-6
		v1.GET("/user-office", strDB.UserOffice)        //get all user and office
		v1.GET("/user-office/:id", strDB.GetUserOffice) //get one user and office
		v1.GET("/user-todo/:id", strDB.UserTodoList)    //get todo list on user

		// todo
		v1.POST("/todo", strDB.CreateTodos)
		v1.DELETE("/todo/:id", strDB.DeleteTodos)
		v1.PUT("/todo/:id", strDB.UpdateTodos)
		v1.GET("/todo/", strDB.GetTodos)
		v1.GET("/todo/:id/show", strDB.GetOneTodos)
		v1.POST("/todo-search", strDB.LoginUser)
		//assesment-6
		v1.GET("/todo-user", strDB.TodosUser)        //get all todo and user
		v1.GET("/todo-user/:id", strDB.GetTodosUser) //get one todo and user

		// redis in controllerRedis
		v1.GET("/without-redis/:current/:perpage", strDB.GetOfficeWithoutRedis)
		v1.GET("/with-redis/:current/:perpage", strDB.GetOfficeWithRedis)
	}
	router.Run(":8080")
	// SendRedis()
}

// SendRedis Comment.
// func SendRedis() {
// 	pool := redis.NewPool(
// 		func() (redis.Conn, error) {
// 			return redis.Dial("tcp", "127.0.0.1:6379")
// 		},
// 		10,
// 	)

// 	pool.MaxActive = 0

// 	//mengambil satu koneksi dari pool
// 	conn := pool.Get()
// 	defer conn.Close()

// 	//conn, err := redis.Dial("tcp", "localhost:6379")

// 	// _, err := conn.Do("HSET", "mahasiswa:5", "nama", "Redha Juanda", "nim", "4sdadad", "ipk", 3.34, "semester", 4)
// 	// if err != nil {
// 	// 	fmt.Println(err.Error)
// 	// }

// 	// _, err = conn.Do("SET", "test", "oke")
// 	// if err != nil {
// 	// 	fmt.Println(err.Error)
// 	// }

// 	// reply, err := redis.Bytes(conn.Do("HGET", "mahasiswa:1", "nim"))
// 	// if err == nil {
// 	// 	fmt.Println(string(reply))
// 	// } else {
// 	// 	fmt.Println(err.Error)
// 	// }

// 	user := map[string]string{
// 		"nama": "cahyo",
// 		"kota": "balikpapan",
// 	}

// 	// map to json
// 	jd, _ := json.Marshal(user)
// 	fmt.Println(string(jd))

// 	// send json to redis
// 	_, _ = conn.Do("SET", "mahasiswa:1", string(jd))

// 	// get json from redis
// 	reply, err := redis.Bytes(conn.Do("GET", "mahasiswa:1"))
// 	if err == nil {
// 		fmt.Println(reply)
// 	}

// 	// make map definition with interface
// 	var sec map[string]interface{}
// 	err = json.Unmarshal(reply, &sec)
// 	if err != nil {
// 		fmt.Println(err)
// 	}
// 	fmt.Println(sec)
// 	jd, _ = json.Marshal(sec)
// 	fmt.Println(string(jd))

// 	_, _ = conn.Do("EXPIRE", "mahasiswa:1", "5")
// }
